
# ####################### Version 2.1, 2017/2/19  ########################
# add load_xls
# add remove_duplicate
# add counter_duplicate
# add array_split

import xlrd
import xlwt
import csv
import numpy as np


def load_xls(filename, sheet_ind):
    """
    load the xls or xlsx file and store in xls_data variable.
    Input parameter is filename and excel sheet index.
    """
    book_type = xlrd.open_workbook(filename)
    sheet_in = book_type.sheet_by_index(sheet_ind)
    xls_data = []
    for i in range(sheet_in.nrows):
        line_data = []
        for j in range(sheet_in.ncols):
            line_data.append(sheet_in.cell(i, j).value)
        xls_data.append(line_data)
    return xls_data


def remove_duplicate(data_in):
    """
    Remove the duplicated data from list 'data_in'.
    """
    data_out = []
    for data in data_in:
        dup = 0
        for data_j in data_out:
            if data == data_j:
                dup = 1
                break

        if dup == 0:
            data_out.append(data)

    return data_out


def counter_duplicate(data_in):
    """
    Calculate the duplicated number for every items in 'data_in'
    """
    data_out = []
    for data in data_in:
        dup = 0
        for data_j in data_out:
            if data == data_j[0]:
                dup = 1
                data_j[1] += 1
                break

        if dup == 0:
            data_out.append([data, 1])

    return data_out


def save_xls(filename, data):
    """
    Save the given data 'data' to xls file as 'filename'
    """
    book_out = xlwt.Workbook(encoding="utf-8")
    sheet_out = book_out.add_sheet("sheet1")
    length = data.__len__()
    for i in xrange(length):
        list_data = data[i]
        for j in xrange(list_data.__len__()):
            sheet_out.write(i, j, list_data[j])

    book_out.save(filename)


def duplicate_list(matrix, item, col_ind):
    """
    Search the duplicated data as item from matrix and output the result
    """
    for mat_list in matrix:
        if mat_list[col_ind] == item:
            return mat_list
            break
