# README #

This project is configured 2 python file(demult.py, func.py) and xlsx documents.
The main executive file is demult.py.

### How do I get set up? ###

We can execute this project two ways.
1. Run with pycharm.

If we open this project with pycharm, we can see the filename set part in line 14.

        file_name = 'H_KW'

In here, we can set the filename.
(note: we should set the filename without file extension ".xlsx")


2. Run with console.

We can open and set the path in command prompt or terminal, and set the such as command line.

        python demult.py filename

for example,

        python demult.py H_KW

or

        python demult.py H_KW.xlsx

(note: In this case the filename don't include space such as "H AD".)

Thanks.
