
# #######################################################################################
#               This Project is Demultiply the base key from Key sets.                  #
# #######################################################################################

import itertools
import func
import xlwt
import sys

""" Input the filename as input argument. """

if len(sys.argv) == 1:
    file_name = 'H_KW'
else:
    file_name = sys.argv[1]
    if file_name[-5:] == '.xlsx':
        file_name = file_name[:-5]


def key_conv(data_split, data_key):
    """
        Replace the key[0] data in data_split to key[1]
        This is necessary for splited one key such as "New York".
    """
    data = data_split
    for key in data_key:
        a = data_split.find(key[0])
        if a != -1:
            data = data_split.replace(key[0], key[1])

    return data


def key_deconv(data, data_key):
    """
        Reverse module of key_conv.
        Replace the key[1] data in data_split to key[0]
    """
    data1 = data
    for key in data_key:
        a = data.find(key[1])
        if a != -1:
            data1 = data.replace(key[1], key[0])

    return data1


def array_split(array, split_col, split_char):
    """
        Split the split_col data in array depend on split_char.
    """
    split_list = []
    for data in array:
        str1 = data[split_col]
        str2 = key_conv(str1, data_key)
        split_list.append(str2.split(split_char))

    return split_list


def add_list(data_set, stack_in, ind1, ind2):
    """
        Add the data list index as stack list output the results and it's counter.
    """
    stack_out = []
    cnt = 0
    for stack_in_item in stack_in:
        for data_item in data_set:
            if stack_in_item == data_item[ind1]:
                stack_out.append(data_item[ind2])
                cnt += 1

    return func.remove_duplicate(stack_out), cnt


def get_string(data_string, data_ind):
    """
        Get the string data from data array list and output the results.
    """
    func_ret = []
    for index in data_ind:
        if data_string[0].__len__() == 1:
            func_ret.append(data_string[index])
        else:
            func_ret.append(data_string[index][0])
    return func_ret


def get_split(data_s, len, ind):
    """
        get the splited data from data_s which length is len
    """
    d_split = []
    for data_i in data_s:
        if data_i.__len__() == len:
            d_split.append(data_i[ind])
    return d_split


def get_split_num(data, len, dcnt1, dcnt2, dcnt3, dcnt4):
    """
        get the split numeric array from raw data list
    """
    ret = []
    for d_i in data:
        if d_i.__len__() == len == 2:
            """ Case of length is 2 """
            for i in range(dcnt1.__len__()):
                if d_i[0] == dcnt1[i][0]:
                    break
            for j in range(dcnt2.__len__()):
                if d_i[1] == dcnt2[j][0]:
                    break
            ret.append([i, j])
        elif d_i.__len__() == len == 3:
            """ Case of length is 33 """
            for i in range(dcnt1.__len__()):
                if d_i[0] == dcnt1[i][0]:
                    break
            for j in range(dcnt2.__len__()):
                if d_i[1] == dcnt2[j][0]:
                    break
            for k in range(dcnt3.__len__()):
                if d_i[2] == dcnt3[k][0]:
                    break
            ret.append([i, j, k])
        elif d_i.__len__() == len == 4:
            """ Case of length is 4 """
            for i in range(dcnt1.__len__()):
                if d_i[0] == dcnt1[i][0]:
                    break
            for j in range(dcnt2.__len__()):
                if d_i[1] == dcnt2[j][0]:
                    break
            for k in range(dcnt3.__len__()):
                if d_i[2] == dcnt3[k][0]:
                    break
            for l in range(dcnt4.__len__()):
                if d_i[3] == dcnt4[l][0]:
                    break
            ret.append([i, j, k, l])

    return ret


def get_stack(d_split_num, len, dcnt1):
    """
        Divide the split numeric data as interacted data sets.
    """
    x_list = []
    end_flag = 0
    data_stack1 = []
    data_stack2 = []
    data_stack3 = []
    data_stack4 = []
    while end_flag == 0:
        """         Check the end point        """
        end_flag = 1
        for i in range(dcnt1.__len__()):
            confirm = 0
            for j in x_list:
                if i == j:
                    confirm = 1
                    break

            if confirm == 0:
                stack1 = [i]
                end_flag = 0
                break

        """     Get the stack data process     """
        if end_flag == 0:
            stack2 = stack3 = stack4 = []
            cnt1 = 0
            cnt2 = -1
            cnt3 = -1
            cnt4 = -1

            if len == 2:
                while cnt1 != cnt2:
                    stack2, cnt1 = add_list(d_split_num, stack1, 0, 1)
                    stack_left_temp, cnt2 = add_list(d_split_num, stack2, 1, 0)
                    if stack_left_temp == []:
                        break
                    else:
                        stack1 = stack_left_temp
            elif len == 3:
                while cnt1 != cnt2 or cnt2 != cnt3 or cnt1 != cnt3:
                    stack2, cnt2 = add_list(d_split_num, stack1, 0, 1)
                    stack3, cnt3 = add_list(d_split_num, stack2, 1, 2)
                    stack_left_temp, cnt1 = add_list(d_split_num, stack3, 2, 0)
                    if stack_left_temp == []:
                        break
                    else:
                        stack1 = stack_left_temp
            elif len == 4:
                while not cnt1 == cnt2 == cnt3 == cnt4:
                    stack2, cnt2 = add_list(d_split_num, stack1, 0, 1)
                    stack3, cnt3 = add_list(d_split_num, stack2, 1, 2)
                    stack4, cnt4 = add_list(d_split_num, stack3, 2, 3)
                    stack_left_temp, cnt1 = add_list(d_split_num, stack4, 3, 0)
                    if stack_left_temp == []:
                        break
                    else:
                        stack1 = stack_left_temp

            for i in stack1:
                x_list.append(i)

            data_stack1.append(stack1)
            data_stack2.append(stack2)
            data_stack3.append(stack3)
            data_stack4.append(stack4)

    """ output the results """
    if len == 2:
        return data_stack1, data_stack2
    elif len == 3:
        return data_stack1, data_stack2, data_stack3
    elif len == 4:
        return data_stack1, data_stack2, data_stack3, data_stack4


def process2(data):
    """
        Get the de-multiplied key data in case of combined with 2 keys.
    """

    """ Get the stack data for preparing de-multiply """
    print ("Analyzing data into 2 segments ...")
    data_split1 = get_split(data, 2, 0)
    data_split2 = get_split(data, 2, 1)
    data_cnt1 = func.counter_duplicate(data_split1)
    data_cnt2 = func.counter_duplicate(data_split2)

    data_split_num = get_split_num(data, 2, data_cnt1, data_cnt2, [], [])

    [data_stack1, data_stack2] = get_stack(data_split_num, 2, data_cnt1)

    ret_ok = []
    ret_fail = []

    """ Search all stack data loop """
    #################################################################
    for step2 in range(data_stack1.__len__()):
        print ("step : ", step2)

        """ Change the situation depend on length of data_stack1 and data_stack2 """
        if data_stack1[step2].__len__() < data_stack2[step2].__len__():
            st1 = data_stack1[step2]
            st2 = data_stack2[step2]
            reverse = 0
        else:
            st1 = data_stack2[step2]
            st2 = data_stack1[step2]
            reverse = 1

        """ Arrange the stack ata with reverse state """
        data_stack = []
        for data_i in data_split_num:
            flag1 = 0
            flag2 = 0
            for item1 in st1:
                if item1 == data_i[reverse]:
                    flag1 = 1
                    break
            for item2 in st2:
                if item2 == data_i[1-reverse]:
                    flag2 = 1
                    break
            if flag1 == 1 and flag2 == 1:
                data_stack.append([item1, item2])

        """ De-multiply the key data from only one stack data """
        start_sub = 0
        Ret1 = []
        Ret2 = []
        while data_stack.__len__() > 0:
            ret_xdata = []
            ret_ydata = []
            ret_cnt = 0

            """ Split the stack data which has less than 12 elements """
            if st1.__len__() - start_sub > 12:
                sub_st1 = st1[start_sub:12]
                sub_st2 = st2
                start_sub += 12
            else:
                sub_st1 = st1[start_sub:]
                sub_st2 = st2
                start_sub = 0
                s1 = []
                s2 = []
                for i in data_stack:
                    s1.append(i[0])
                    s2.append(i[1])
                st1 = func.remove_duplicate(s1)
                st2 = func.remove_duplicate(s2)

            """ Search the full connected key pair from stack data """
            for i in range(sub_st1.__len__()):
                comb = list(itertools.combinations(sub_st1, i + 1))
                for comb_i in comb:

                    ret_y = []
                    for comb_y in sub_st2:
                        flag_break = 0
                        for comb_x in comb_i:
                            if not data_stack. __contains__([comb_x, comb_y]):
                                flag_break = 1
                                break
                        if flag_break == 0:
                            ret_y.append(comb_y)

                    if ret_cnt < comb_i.__len__()*ret_y.__len__():
                        ret_xdata = comb_i
                        ret_ydata = ret_y
                        ret_cnt = comb_i.__len__()*ret_y.__len__()

            """ Remove the found key pair from stack data sets """
            for i in ret_xdata:
                for j in ret_ydata:
                    data_stack.remove([i, j])

            """ Set the loop flag depend on stack data sets state """
            if ret_xdata != []:
                flag_dup = 0
                for j in range(Ret2.__len__()):
                    if Ret2[j] == ret_ydata:
                        flag_dup = 1
                        for i in ret_xdata:
                            Ret1[j] = Ret1[j] + (i,)
                        break

                if flag_dup == 0:
                    Ret1.append(ret_xdata)
                    Ret2.append(ret_ydata)

        """ get the real string data from numerical data """
        for i in range(Ret1.__len__()):
            if reverse == 0:
                str1 = get_string(data_cnt1, Ret1[i])
                str2 = get_string(data_cnt2, Ret2[i])
            else:
                str1 = get_string(data_cnt1, Ret2[i])
                str2 = get_string(data_cnt2, Ret1[i])

            if Ret1[i].__len__() == Ret2[i].__len__() == 1:
                ret_fail.append([str1[0] + ' ' + str2[0]])
                ret_fail.append([])
            else:
                ret_ok.append([str1, str2])

    return ret_ok, ret_fail


def process3(data):
    """
        Get the de-multiplied key data in case of combined with 3 keys.
    """

    """ Get the stack data for preparing de-multiply """
    print ("Analyzing data into 3 segments ...")
    data_split1 = []
    data_split2 = []
    data_split3 = []
    for data_i in data:
        if data_i.__len__() == 3:
            data_split1.append(data_i[0])
            data_split2.append(data_i[1])
            data_split3.append(data_i[2])

    data_cnt1 = func.counter_duplicate(data_split1)
    data_cnt2 = func.counter_duplicate(data_split2)
    data_cnt3 = func.counter_duplicate(data_split3)

    """ Convert the string data to numerical data for processing """
    data_split_num = []
    for d_i in data:
        if d_i.__len__() == 3:
            for i in range(data_cnt1.__len__()):
                if d_i[0] == data_cnt1[i][0]:
                    break

            for j in range(data_cnt2.__len__()):
                if d_i[1] == data_cnt2[j][0]:
                    break

            for k in range(data_cnt3.__len__()):
                if d_i[2] == data_cnt3[k][0]:
                    break

            data_split_num.append([i, j, k])

    """ De-multiply loop process """
    x_list = []
    end_flag = 0
    ret = []
    ret_fail = []
    while end_flag == 0:
        """ Check the end flag using stack data """
        end_flag = 1
        for i in range(data_cnt1.__len__()):
            confirm = 0
            for j in x_list:
                if i == j:
                    confirm = 1
                    break

            if confirm == 0:
                stack1 = [i]
                end_flag = 0
                break

        """ De-multiply the stack data to key pair set """
        if end_flag == 0:
            stack2 = stack3 = []
            cnt1 = 0
            cnt2 = -1
            cnt3 = -1

            """ Check the de-multiply condition """
            while cnt1 != cnt2 or cnt2 != cnt3 or cnt1 != cnt3:
                stack2, cnt2 = add_list(data_split_num, stack1, 0, 1)
                stack3, cnt3 = add_list(data_split_num, stack2, 1, 2)
                stack_left_temp, cnt1 = add_list(data_split_num, stack3, 2, 0)

                if stack_left_temp == []:
                    break
                else:
                    stack1 = stack_left_temp

            """ output the results """
            for i in stack1:
                x_list.append(i)

            if cnt1 == 1:
                ret_fail.append([get_string(data_cnt1, stack1), get_string(data_cnt2, stack2), get_string(data_cnt3, stack3)])
            else:
                ret.append([get_string(data_cnt1, stack1), get_string(data_cnt2, stack2), get_string(data_cnt3, stack3)])

    return ret, ret_fail


def process4(data):
    """
        Get the de-multiplied key data in case of combined with 4 keys.
    """

    """ Get the stack data for preparing de-multiply """
    print ("Analyzing data into 4 segments ...")
    data_split1 = []
    data_split2 = []
    data_split3 = []
    data_split4 = []
    for data_i in data:
        if data_i.__len__() == 4:
            data_split1.append(data_i[0])
            data_split2.append(data_i[1])
            data_split3.append(data_i[2])
            data_split4.append(data_i[3])

    data_cnt1 = func.counter_duplicate(data_split1)
    data_cnt2 = func.counter_duplicate(data_split2)
    data_cnt3 = func.counter_duplicate(data_split3)
    data_cnt4 = func.counter_duplicate(data_split4)

    """ Convert the string data to numerical data for processing """
    data_split_num = []
    for d_i in data:
        if d_i.__len__() == 4:
            for i in range(data_cnt1.__len__()):
                if d_i[0] == data_cnt1[i][0]:
                    break

            for j in range(data_cnt2.__len__()):
                if d_i[1] == data_cnt2[j][0]:
                    break

            for k in range(data_cnt3.__len__()):
                if d_i[2] == data_cnt3[k][0]:
                    break

            for l in range(data_cnt4.__len__()):
                if d_i[3] == data_cnt4[l][0]:
                    break

            data_split_num.append([i, j, k, l])

    """ De-multiply loop process """
    x_list = []
    end_flag = 0
    ret = []
    ret_fail = []
    while end_flag == 0:
        """ Check the end flag using stack data """
        end_flag = 1
        for i in range(data_cnt1.__len__()):
            confirm = 0
            for j in x_list:
                if i == j:
                    confirm = 1
                    break

            if confirm == 0:
                stack1 = [i]
                end_flag = 0
                break

        """ De-multiply the stack data to key pair set """
        if end_flag == 0:
            stack2 = stack3 = stack4 = []
            cnt1 = 0
            cnt2 = -1
            cnt3 = -1
            cnt4 = -1

            """ Check the de-multiply condition """
            while not cnt1 == cnt2 == cnt3 == cnt4:
                stack2, cnt2 = add_list(data_split_num, stack1, 0, 1)
                stack3, cnt3 = add_list(data_split_num, stack2, 1, 2)
                stack4, cnt4 = add_list(data_split_num, stack3, 2, 3)
                stack_left_temp, cnt1 = add_list(data_split_num, stack4, 3, 0)

                if stack_left_temp == []:
                    break
                else:
                    stack1 = stack_left_temp

            """ output the results """
            for i in stack1:
                x_list.append(i)

            if stack1.__len__() == stack2.__len__() == stack3.__len__() == stack4.__len__() == 1:
                str1 = get_string(data_cnt1, stack1)
                str2 = get_string(data_cnt2, stack2)
                str3 = get_string(data_cnt3, stack3)
                str4 = get_string(data_cnt4, stack4)
                ret_fail.append([str1[0] + ' ' + str2[0] + ' ' + str3[0] + ' ' + str4[0]])
            else:
                ret.append([get_string(data_cnt1, stack1), get_string(data_cnt2, stack2),
                           get_string(data_cnt3, stack3), get_string(data_cnt4, stack4)])

    return ret, ret_fail


""" Set the input and output file name. """
file_input = file_name + '.xlsx'
file_output = file_name +'_ret.xls'

""" Convert the xls data to raw data and load key data. """
data_org = func.load_xls(file_input, 0)
data_dup = func.remove_duplicate(data_org[1:])
data_key = func.load_xls('key', 0)

""" Write original data to xls document. """
book_out = xlwt.Workbook(encoding="utf-8")
sheet_out = book_out.add_sheet("org")
for i in range(data_org.__len__()):
    for j in range(data_org[i].__len__()):
        sheet_out.write(i, j, data_org[i][j])

""" Write base key data to xls document. """
for row_ind in range(data_dup[0].__len__()):

    """ Get the clustered data for de-multiplying. """
    data_rm = []
    for i in data_dup:
        data_rm.append([i[row_ind]])
    rm = func.remove_duplicate(data_rm)

    data_split = array_split(data_dup, row_ind, ' ')
    data_split = func.remove_duplicate(data_split)
    r2 = []

    """ De-multiplying as 2, 3 and 4 keywords. """
    r2, r2f = process2(data_split)
    r3, r3f = process3(data_split)
    r4, r4f = process4(data_split)
    for i in r3:
        r2.append(i)
    for i in r4:
        r2.append(i)

    """ Write the base-keyword data to excel sheet. """
    sheet_name = "HLP" + (row_ind+1).__str__()
    for key_ind in range(r2.__len__()):
        """ Add new sheet. """
        if key_ind < 26:
            sheet_out = book_out.add_sheet(sheet_name + chr(97 + key_ind))
        elif key_ind < 52:
            sheet_out = book_out.add_sheet(sheet_name + 'a' + chr(71 + key_ind))
        elif key_ind < 78:
            sheet_out = book_out.add_sheet(sheet_name + 'b' + chr(71 + key_ind))
        else:
            sheet_out = book_out.add_sheet(sheet_name + 'c' + chr(71 + key_ind))

        """ Write the key data to sheet. """
        for i in range(r2[key_ind].__len__()):
            for j in range(r2[key_ind][i].__len__()):
                sheet_out.write(j, i, key_deconv(r2[key_ind][i][j], data_key))

    if r2 == []:
        sheet_out = book_out.add_sheet("HLP" + (row_ind + 1).__str__() + 'a')
        for i in range(rm.__len__()):
            sheet_out.write(i, 0, rm[i])

book_out.save(file_output)
